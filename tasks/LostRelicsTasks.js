const { task } = require("hardhat/config");

const CONTRACT_NAME = "LostRelics";

task("getSeasonStats", "Season loot stats")
  .addParam("contract", "The contract's address")
  .setAction(async (taskArgs) => {
    const contractAddr = taskArgs.contract;
    const LostRelics = await ethers.getContractFactory(CONTRACT_NAME);

    const accounts = await ethers.getSigners();
    const deployer = accounts[0];
    const lrc = await LostRelics.attach(contractAddr);

    // TODO This is basic, get it working version.
    //      When LostRelics has more state variables, more stats can be obtained.
    let unclaimed = await lrc.balanceOf(deployer.address, lrc.CACHE());
    let claimed = 10 - unclaimed;
    console.log('Cache stats:');
    console.log(`\tClaimed: ${claimed}`);
    console.log(`\tUnclaimed: ${unclaimed}`);
  });

task("buyCache", "Buy a Cache")
  .addParam("contract", "The contract's address")
  .setAction(async (taskArgs) => {
    const contractAddr = taskArgs.contract;
    const LostRelics = await ethers.getContractFactory(CONTRACT_NAME);

    const [deployer, alice] = await ethers.getSigners();
    const lrc = await LostRelics.attach(contractAddr);

    // "buy" a cache
    const txn = await lrc.safeTransferFrom(
        deployer.address,
        alice.address,
        lrc.CACHE(),
        1,
        "0x"
    );

    const balance = await lrc.balanceOf(alice.address, lrc.CACHE());
    console.log('Cache purchase complete');
    console.log(`\tAddress: ${alice.address}`);
    console.log(`\tCache balance: ${balance}`);
  });

task("getCollection", "View your Relics and Caches")
  .addParam("contract", "The contract's address")
  .addParam("owner", "The collection owner's address")
  .setAction(async (taskArgs) => {
    const contractAddr = taskArgs.contract;
    const ownerAddr = taskArgs.owner;
    const LostRelics = await ethers.getContractFactory(CONTRACT_NAME);

    const lrc = await LostRelics.attach(contractAddr);

    const tokenNames = ["Cache", "Common Weapon", "Common Armor", "Rare Weapon", "Rare Armor"];
    const tokenIds = [0, 100, 101, 200, 201];
    const accountIds = [ownerAddr, ownerAddr, ownerAddr, ownerAddr, ownerAddr];
    const collection = await lrc.balanceOfBatch(accountIds, tokenIds);
    console.log("Your collection balance is...");
    for (let i =0; i < collection.length; i++) {
        let itemName = tokenNames[i];
        let itemQty = collection[i];
        console.log(`\t${itemName} is: ${itemQty}`);
    }
  });

task("unlockCache", "Unlock a Cache")
  .addParam("contract", "The contract's address")
  .addParam("owner", "The collection owner's address")
  .setAction(async (taskArgs) => {
    const contractAddr = taskArgs.contract;
    const ownerAddr = taskArgs.owner;
    const LostRelics = await ethers.getContractFactory(CONTRACT_NAME);

    const lrc = await LostRelics.attach(contractAddr);

    // Get the signer object for the address.
    const signers = await ethers.getSigners();
    let owner;
    for (i = 0; i < signers.length; i++) {
        if (signers[i].address === ownerAddr) {
            owner = signers[i];
            break;
        }
    }

    if (owner) {
        console.log("Unlocking cache");
        await lrc.connect(owner).unlockCache();
        console.log("Cache unlocked, try viewing collection again");
    } else {
        console.log("No Signer found for address provided");
    }
  });

task("grantRoles", "Grant access to minting, pausing and URI capabilities")
  .addParam("contract", "The contract's address")
  .addParam("address", "The addres roles will be granted to")
  .setAction(async (taskArgs) => {
    const contractAddr = taskArgs.contract;
    const granteeAddr = taskArgs.address;

    const LostRelics = await ethers.getContractFactory(CONTRACT_NAME);
    const lrc = await LostRelics.attach(contractAddr);

    const roles = [
      '0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6', // mint, mintBatch
      '0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a', // pause, unpause
      '0x7804d923f43a17d325d77e781528e0793b2edd9890ab45fc64efd7b4b427744c'  // setURI
    ];

    for (let i = 0; i < roles.length; i++) {
      console.log(`Granting role: ${roles[i]}`);
      await lrc.grantRole(roles[i], granteeAddr);
      console.log('...granted');
    }

    for (let i = 0; i < roles.length; i++) {
      let hasRole = await lrc.hasRole(roles[i], granteeAddr);
      console.log(`Role granted [${hasRole}] for ${roles[i]}`);
    }
  });

/*
task("name", "Transfer Relic")
  .addParam("contract", "The contract's address")
  .setAction(async (taskArgs) => {});

task("name", "Transfer Cache")
  .addParam("contract", "The contract's address");

  .setAction(async (taskArgs) => {});
*/
module.exports = {};
