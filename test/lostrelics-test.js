const { expect } = require("chai");
const { ethers } = require("hardhat");

// Contract factory, and contract instance
let ContractFactory, contract;
// n-Signers created by Hardhat network
let [deployer, alice, bob, chad] = "";

describe("LostRelics", function () {
  /**
   * Initialize:
   * - Contract factory
   * - Contract instance
   * - The n-Signers needed for various test cases
   */
  beforeEach(async function () {
    ContractFactory = await ethers.getContractFactory("LostRelics");
    contract = await ContractFactory.deploy();
    await contract.deployed();
    [deployer, alice, bob, chad] = await ethers.getSigners();
  });

  it("Should mint initial Cache tokens when deployed", async function () {
    expect(await contract.balanceOf(deployer.address, contract.CACHE())).to.equal(10);
  });

  it("Should mint initial Common item tokens when deployed", async function () {
    expect(await contract.balanceOf(deployer.address, contract.COMMON_WEAPON())).to.equal(100);
    expect(await contract.balanceOf(deployer.address, contract.COMMON_ARMOR())).to.equal(100);
  });

  it("Should mint initial Rare item tokens when deployed", async function () {
    expect(await contract.balanceOf(deployer.address, contract.RARE_WEAPON())).to.equal(20);
    expect(await contract.balanceOf(deployer.address, contract.RARE_ARMOR())).to.equal(20);
  });

  it("Should prevent unlock when no Caches are owned", async function () {
    await expect(contract.connect(alice).unlockCache()).to.be.revertedWith(
      "ERC1155: burn amount exceeds balance"
    );
  });

  it("Should burn Cache when unlocked", async function () {
    await contract.safeTransferFrom(
      deployer.address,
      alice.address,
      contract.CACHE(),
      1,
      "0x"
    );
    expect(await contract.balanceOf(alice.address, contract.CACHE())).to.equal(1);

    await contract.connect(alice).unlockCache();

    expect(await contract.balanceOf(alice.address, contract.CACHE())).to.equal(0);
  });

  it("Should distribute Common items when unlocked", async function () {
    await contract.safeTransferFrom(
      deployer.address,
      alice.address,
      contract.CACHE(),
      1,
      "0x"
    );
    expect(await contract.balanceOf(alice.address, contract.CACHE())).to.equal(1);

    await contract.connect(alice).unlockCache();

    expect(await contract.balanceOf(alice.address, contract.COMMON_ARMOR())).to.equal(10);
    expect(await contract.balanceOf(alice.address, contract.COMMON_WEAPON())).to.equal(10);
    expect(await contract.balanceOf(deployer.address, contract.COMMON_ARMOR())).to.equal(90);
    expect(await contract.balanceOf(deployer.address, contract.COMMON_WEAPON())).to.equal(90);
  });

  it("Should distribute Common items and decrement Cache balance when unlocked", async function () {
    await contract.safeTransferFrom(
      deployer.address,
      alice.address,
      contract.CACHE(),
      2,
      "0x"
    );

    expect(await contract.balanceOf(alice.address, contract.CACHE())).to.equal(2);

    // alice will now unlock 1 cache...
    await contract.connect(alice).unlockCache();

    // after unlock, alice should have 1 cache, and 10 common armor, 10 common weapon
    expect(await contract.balanceOf(alice.address, contract.CACHE())).to.equal(1);
    expect(await contract.balanceOf(alice.address, contract.COMMON_ARMOR())).to.equal(10);
    expect(await contract.balanceOf(alice.address, contract.COMMON_WEAPON())).to.equal(10);

    // after unlock, original owner of all loot should have 8 caches, 90 or each common type
    expect(await contract.balanceOf(deployer.address, contract.CACHE())).to.equal(8);  
    expect(await contract.balanceOf(deployer.address, contract.COMMON_ARMOR())).to.equal(90);
    expect(await contract.balanceOf(deployer.address, contract.COMMON_WEAPON())).to.equal(90);
  });

  it("Should allow deployer to mint", async function (){
    expect(await contract.balanceOf(deployer.address, contract.CACHE())).to.equal(10);
    await contract.mint(deployer.address, contract.CACHE(), 1, "0x");
    expect(await contract.balanceOf(deployer.address, contract.CACHE())).to.equal(11);
  });

  it("Should require role to mint", async function (){
    await expect(contract.connect(alice).mint(alice.address, 0, 1, "0x")).to.be.reverted;
  });

  it("Should allow mint once granted role", async function (){
    expect(await contract.balanceOf(alice.address, contract.CACHE())).to.equal(0);
    await contract.grantRole('0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6', alice.address);
    await contract.connect(alice).mint(alice.address, contract.CACHE(), 1, "0x");
    expect(await contract.balanceOf(alice.address, contract.CACHE())).to.equal(1);
  });

  it("Should require role to pause", async function (){
    await expect(contract.connect(alice).pause()).to.be.reverted;
  });

  it("Should allow pause once granted role", async function (){
    await contract.grantRole('0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a', alice.address);
    await contract.connect(alice).pause();
  });

  it("Should require role to unpause", async function (){
    await expect(contract.connect(alice).unpause()).to.be.reverted;
  });

  it("Should allow unpause once granted role", async function (){
    await contract.grantRole('0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a', alice.address);
    await contract.connect(alice).pause();
    await contract.connect(alice).unpause();
  });

  it("Should require role to set URI", async function (){
    await expect(contract.connect(alice).setURI("https://someuri.example.com")).to.be.reverted;
  });

  it("Should allow set URI once granted role", async function (){
    await contract.grantRole('0x7804d923f43a17d325d77e781528e0793b2edd9890ab45fc64efd7b4b427744c', alice.address);
    await contract.connect(alice).setURI("https://someuri.example.com");
  });
});
