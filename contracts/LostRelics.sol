// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Burnable.sol";

contract LostRelics is ERC1155, AccessControl, Ownable, Pausable, ERC1155Burnable {
    bytes32 public constant URI_SETTER_ROLE = keccak256("URI_SETTER_ROLE");
    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    // TODO We could use variables to track basic season stats
    //      e.g., # caches/items created/claimed
    // TODO Is there any utility from implied "tiers" or ranges of IDs?
    uint256 public constant CACHE = 0;

    // NOTE Trying out concept to generalize items < Epic / Legendary.
    //      The rarity tiers I'm assuming right now are common < rare < epic < legendary.
    //      This may keep it simple, and also allow player's imagination to fill in details.
    uint256 public constant COMMON_WEAPON = 100;
    uint256 public constant COMMON_ARMOR = 101;
    uint256 public constant RARE_WEAPON = 200;
    uint256 public constant RARE_ARMOR = 201;

    // NOTE Here's an example of a unique 1/1 item
    //      These could be minted post-deployment also, as new items are "forged"
    // uint256 public constant SWORD_OF_AWESOME = 1000;

    // TODO Need metadata URI
    constructor() ERC1155("https://xhnlfaxfq2wa.usemoralis.com/season1/{id}.json") {
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setupRole(URI_SETTER_ROLE, msg.sender);
        _setupRole(PAUSER_ROLE, msg.sender);
        _setupRole(MINTER_ROLE, msg.sender);
        _mint(msg.sender, CACHE, 10, "");
        _mint(msg.sender, COMMON_WEAPON, 100, "");
        _mint(msg.sender, COMMON_ARMOR, 100, "");
        _mint(msg.sender, RARE_WEAPON, 20, "");
        _mint(msg.sender, RARE_ARMOR, 20, "");
    }

    function setURI(string memory newuri) public onlyRole(URI_SETTER_ROLE) {
        _setURI(newuri);
    }

    function pause() public onlyRole(PAUSER_ROLE) {
        _pause();
    }

    function unpause() public onlyRole(PAUSER_ROLE) {
        _unpause();
    }

    function mint(address account, uint256 id, uint256 amount, bytes memory data)
        public
        onlyRole(MINTER_ROLE)
    {
        _mint(account, id, amount, data);
    }

    function mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data)
        public
        onlyRole(MINTER_ROLE)
    {
        _mintBatch(to, ids, amounts, data);
    }

    function _beforeTokenTransfer(address operator, address from, address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data)
        internal
        whenNotPaused
        override
    {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);
    }

    function unlockCache() public {
        // NOTE There's likely a whole bunch of state problems here, need to review the order of operations


        // NOTE Since we're dealing w/ fungible Caches, the act of unlocking destroys the Cache.
        //      There's no NFT locked state being tracked, and nothing to update.
        //      At this point, I think the options are to burn the Cache, or transfer
        //      ownership back to LostRelics. The second option seems prone to abuse, and runs
        //      counter to the scarcity concept
        _burn(msg.sender, CACHE, 1);

        // TODO This is where we'd intelligently transfer Relics to msg.sender. This implemenation
        //      is basic sanity check on the ideas I had as to how an unlock -> xfer might work.
        // TODO There's got to be a better way to pass dynamic array of ids, and quantities in Solidity
        uint256[] memory gear = new uint256[](2);
        gear[0] = COMMON_WEAPON;
        gear[1] = COMMON_ARMOR;

        uint256[] memory qty = new uint256[](2);
        qty[0] = 10;
        qty[1] = 10;

        // TODO If this xfer fails, did we just burn the Cache and give them nothing?
        //      I think I can write a simple test for this and try to xfer w/ insufficient funds to
        //      trigger an xfer revert, then could check the balance of Caches to see if the burn
        //      was also reverted.
        // TODO If ownership is transferred to another address, this will all come crashing down
        _safeBatchTransferFrom(owner(), msg.sender, gear, qty, '');
    
        /* TODO We could tie into Chainlink VRF to determine range of loot rarity, and select more unique
         *      Relics to distribute. Keeping it simple for v1.
         *      
         *      Basic Relic distribution idea using Chainlink VRF...
         *      Given an address that owns a Cache
         *      When unlockCache is called
         *      and LostRelics makes a successful request for randomness
         *      and fulfillRandomness is called
         *      Then...
         *
         *      We can obtain the "rarity" or the Cache by, vrf_result % 100
         *      This will produce rarity value 0-99.
         *      We apply a rarity distribution; e.g. 0-79 rare, 80-94 epic, 95-99 legendary
         *      If we've pre-minted Relics, we need to ensure sufficient supply of each tier was produced.
         *      These would ideally be unique NFTs, so we'd need to select an available Relic from 
         *      the pool of Relics. This should be more random than array.pop(), otherwise it's too
         *      easy to guess what you'll get (there should be some mystery here!).
         *      If we were using ERC721 NFTs, we could simply generate a Relic.
         *      We can explore that, too -- But we decided to continue focusing on the idea of using
         *      ERC1155 && pre-minting all the Relics, so let's keep exploring ideas based on that choice.
         */
    }

    // The following functions are overrides required by Solidity.

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC1155, AccessControl)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

}