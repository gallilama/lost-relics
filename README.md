# Lost Relics

## Project Intro

### Summary

Lost Relics explores how [tabletop role-playing games](https://en.wikipedia.org/wiki/Tabletop_role-playing_game) could leverage Smart Contracts, NFTs and Oracles to bring communities of gamers together to create shared stories, build a connected world, encounter new player and game experiences, and earn loot.

### Our Adventure's Backstory

Lost Relics is envisioned as a series or campaign based experience capable of supporting many "seasons" of adventures. The hackathon project will initiate season one: Lost Relics of Anari. The project will "set the stage" for the season by outlining enough backstory to support clues and enable GMs to create interesting adventures which tie into a larger storyline.

TODO: This needs serious word-smithing. It needs to be short-n-sweet. But here goes...

Anari was an Artificier in the world of `FANCY_WORLD_NAME` who created exceptional items of magical and special abilities which had no equal. At `SOME_REALLY_BAD_EVENT`, Anari decided to protect these artifcats by creating Relic Caches. The Relic Caches were hidden throughout `FANCY_WORLD_NAME`. Over time, all knowledge of Anari and the Relic Caches was lost, until now. Recent clues to the locations of several Relic Caches have emerged. Obtain one or more of these clues, assemble a party of adventurers, and brave the dungeons to claim these lost treasures.

Help shape the story of `FANCY_WORLD_NAME`, build and interact with a larger role-playing community, and mint epic loot. The next adventure will be created by you.

### Questions to Explore

- How can decentralized, open platforms and technologies enable new tabletop role-playing stories and experiences, and bring role-playing communities together?
  - TTRPG technology is based on several walled gardens of proprietary VTT.
  - This also reinforces separated worlds / games / etc.
- How can Smart Contracts, NFTs, Oracles, etc may enable game experiences such as:
  - Unique NFT adventures featuring custom maps / art / more in shared world.
  - Engage content creators through NFT Adventures (e.g. dungeon map designer, backstory writers, visual artists, etc)
  - Unique NFT loot w/ world-level scarcity / rarity via Smart Contract algorithm / NFT minting process upon Adventure completion
  - Transferrable loot (e.g. Town Trading Post), risk-reward mechanics (e.g. trasfer of loot upon character death to arch villian dugneon owner)
  - Oracles to record adventures / outcomes, and update on-chain state of dynamic NFTs
  - Enable different types of role-playing; e.g., RP a rare collector of world loot artifacts, employ a party of Adevnturers to tackle a dungeon to obtain NFT loot, and compensate the party with ETH / other crypto / or possibily a RPG utility coin.
- What else will learn on this adventure that we don't even know we don't know?

### Why this project?

The team are builders _and_ gamers. RPG + blockchain presented an intersection of technical interest and real world experience as Players / GMs -- a compelling combination from which the team could explore and learn blockchain technology and concepts, while being grounded in a problem domain the team was both experienced and interested in.

## Roadmap / Milestones

We will use the names and labels of the orginal D&D basic set adventures to organize the project's milestones.

### B1 In Search of the Unknown

The first milestone should tackle fundamental concepts, and tackel big questions.

- Establish world backstory and Adventure + NFT "macguffin"
- Prove the basic end-to-end experience of:
  - Obtain NFT Cache with simple backstory to enable GM to create adeventure to unlock Relic Cache which ties into larger Anari backstory
  - Complete the Adventure to unlock Relic Cache
  - Open Relic Cache to mint Relics
- Relic minting/distribution algorithm (item type, attributes, abilities, rarity, art, etc)
- Basic NFT portal app to view character / loot

### B2 The Keep on the Borderlands

With the fundamnental mechanics in place, it's time to incorporate community / player experience

- Off-chain adventure log / outcome (an Oracle of sorts)
- Adventure outcome influencing on-chain, dynamic NFTs (e.g. TPK could result in xfer of loot to "evil dungeon overlord")
- NFT based Adventure assets (e.g., map, art, puzzles, etc) to supplement Clues
- Player app to examine loot, adventure log, activities of other adventuring parties in the world
- Stats of Clues found, Adventures completed, etc
- Town Trading Post experience to enable Loot Item buy / sell / trade

### B3 Palace of the Silver Princess

We'll have discovered some things along the way, pivoted, and such. Perhaps we'll have time to create new ideas / update / improve.

- Role-playing as artifact collector and rewarding adventuring party with crypto currency or utility token (DATs - Dungeon Adventure Tokens, etc).

### B4..Bn

Placeholders if needed. We've got big dreams!

- How to make loot extensible to grow w/ the Character: Upgrade relic w/ another; e.g. a gem to upgrade a sword
- There could be a time mechanic in-game for the season. On date-x there's a new game mechanic introduced into the season.

## Setup & Run
We're learning a great deal, and capturing key commands in this section. It's likely to get a bit messy in this section for a bit.

### Install & Config
To get very basics running do this:

- clone repo && cd project_dir
- set RINKEBY_RPC_URL, PRIVATE_KEY (metamask account private key) in project_dir/.env
- npm install
- npx hardhat test

### LostRelics v1 end-to-end via CLI
Showing basic end-to-end example for LostRelics, using Hardhat CLI tasks on localhost network.
```
// start localhost network in terminal #2
$ npx hardhat node

// 
// now back to terminal #1
//

// deploy contract
$ npx hardhat run --network localhost scripts/deploy-lost-relics.js
LostRelics deployed to: 0x5FbDB2315678afecb367f032d93F642f64180aa3

// get initial season stats
$ npx hardhat getSeasonStats --network localhost --contract 0x5FbDB2315678afecb367f032d93F642f64180aa3
Cache stats:
        Claimed: 0
        Unclaimed: 10

// have accounts[1], aka 'alice' buy a Cache
$ npx hardhat buyCache --network localhost --contract 0x5FbDB2315678afecb367f032d93F642f64180aa3
Cache purchase complete
        Address: 0x70997970C51812dc3A010C7d01b50e0d17dc79C8
        Cache balance: 1

// get updated seasons stats, there should be 1 claimed Cache
$ npx hardhat getSeasonStats --network localhost --contract 0x5FbDB2315678afecb367f032d93F642f64180aa3
Cache stats:
        Claimed: 1
        Unclaimed: 9

// get collection
$ npx hardhat getCollection --network localhost \
--contract 0x5FbDB2315678afecb367f032d93F642f64180aa3 \
--owner 0x70997970C51812dc3A010C7d01b50e0d17dc79C8

// unlock Cache
$ npx hardhat unlockCache --network localhost \
--contract 0x5FbDB2315678afecb367f032d93F642f64180aa3 \
--owner 0x70997970C51812dc3A010C7d01b50e0d17dc79C8
```

### Moralis Stuff
This is a place for me to put how I setup Moralis for now.

* JSON meta files need to be named a 64 charact hex representation(lower case!!!) of the number assigned to the NFT (i.e. 10 becomes 000000000000000000000000000000000000000000000000000000000000000a.json)

Moralis Server folder: /metadata-webapp
Moralis server name: Lost-Relics-Devchain
Moralis server network: Local Devchain
.env file settings:
```shell
MORALIS_API_KEY=
MORALIS_PRIVATE_KEY=
MORALIS_SITE=https://yh2cfmmzo4yx.usemoralis.com
```

To upload to the server:
```shell
npm install moralis-admin-cli
cd metadata-webapp
npx moralis-admin-cli deploy
```

1. After the upload, grab the URls and update the ###.json files with the correct url
2. Update the URL in the constructor for ERC1155

Setup local frp:

We include a built-in proxy server to allow connection to your local devchain instance, just follow the steps bellow and in less that 5 minutes you will be all set-up.

1. Download the version required depending on your hardware / os
https://github.com/fatedier/frp/releases

2. Replace the following content in "frpc.ini", based on your devchain
Hardhat:

```shell
[common]
  server_addr = yh2cfmmzo4yx.usemoralis.com
  server_port = 7000
  token = 1MhMtaHI8L
[hardhat]
  type = http
  local_port = 8545
  custom_domains = yh2cfmmzo4yx.usemoralis.com
```

Start up the proxy:
```shell
//make sure your local node is running
npx hardhat node
//in another terminal 
./frpc -c frpc.ini
```
Mac / Windows Troubleshooting: https://docs.moralis.io/faq#frpc

### React Website

Website project moved to: https://gitlab.com/freedonnadd71/lost-relics-webapp

## License

Lost Relics is released under the [MIT License](LICENSE).
